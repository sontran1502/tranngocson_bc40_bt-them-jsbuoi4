//exercise 1

function ngayHomQua() {
    var dd = document.getElementById("txt-ngay").value * 1;
    var mm = document.getElementById("txt-thang").value * 1;
    var yy = document.getElementById("txt-nam").value * 1;
    var result1 = document.getElementById("result1");
    if (yy >= 1920) {
        if (1 <= mm && mm <= 12) {
            if (mm == 5 || mm == 7 || mm == 10 || mm == 12) {
                if (dd == 1) {
                    dd = 30;
                    mm -= 1;
                    result1.innerHTML = ` <h2 class=" mt-3 text-danger text-left ">
                     ${dd} / ${mm} / ${yy}
                    </h2>`
                } else if (1 < dd && dd <= 31) {
                    dd -= 1;
                    result1.innerHTML = ` <h2 class=" mt-3 text-danger text-left ">
                    ${dd} / ${mm} / ${yy}
                   </h2>`
                } else {
                    alert("Dữ Liệu Không Hợp Lệ");
                }
            } else {
                if (mm == 1) {
                    if (dd == 1) {
                        dd = 31;
                        mm = 12;
                        yy -= 1;
                        result1.innerHTML = ` <h2 class=" mt-3 text-danger text-left ">
                        ${dd} / ${mm} / ${yy}
                       </h2>`

                    } else if (1 < dd && dd <= 31) {
                        dd -= 1;
                        result1.innerHTML = ` <h2 class=" mt-3 text-danger text-left ">
                        ${dd} / ${mm} / ${yy}
                       </h2>`
                    } else {
                        alert("Dữ Liệu Không Hợp Lệ");
                    }
                } else if (mm == 3) {
                    if ((yy % 4 == 0 && yy % 100 != 0) || yy % 400 == 0) {
                        if (dd == 1) {
                            dd = 29;
                            mm -= 1;
                            result1.innerHTML = ` <h2 class=" mt-3 text-danger text-left ">
                            ${dd} / ${mm} / ${yy}
                           </h2>`
                        } else if (1 < dd && dd <= 31) {
                            dd -= 1;
                            result1.innerHTML = ` <h2 class=" mt-3 text-danger text-left ">
                            ${dd} / ${mm} / ${yy}
                           </h2>`
                        } else {
                            alert("Dữ Liệu Không Hợp Lệ");
                        }
                    } else {
                        if (dd == 1) {
                            dd = 28;
                            mm -= 1;
                            result1.innerHTML = ` <h2 class=" mt-3 text-danger text-left ">
                            ${dd} / ${mm} / ${yy}
                           </h2>`
                        } else if (1 < dd && dd <= 31) {
                            dd -= 1;
                            result1.innerHTML = ` <h2 class=" mt-3 text-danger text-left ">
                            ${dd} / ${mm} / ${yy}
                           </h2>`
                        } else {
                            alert("Dữ Liệu Không Hợp Lệ");
                        }
                    }

                } else if (mm == 2) {
                    if ((yy % 4 == 0 && yy % 100 != 0) || yy % 400 == 0) {
                        if (dd == 1) {
                            dd = 31;
                            mm = 1;
                            result1.innerHTML = ` <h2 class=" mt-3 text-danger text-left ">
                            ${dd} / ${mm} / ${yy}
                           </h2>`
                        } else if (1 < dd && dd <= 29) {
                            dd -= 1;
                            result1.innerHTML = ` <h2 class=" mt-3 text-danger text-left ">
                            ${dd} / ${mm} / ${yy}
                           </h2>`
                        } else {
                            alert("Dữ Liệu Không Hợp Lệ");
                        }
                    } else {
                        if (dd == 1) {
                            dd = 31;
                            mm = 1;
                            result1.innerHTML = ` <h2 class=" mt-3 text-danger text-left ">
                            ${dd} / ${mm} / ${yy}
                           </h2>`
                        } else if (1 < dd && dd <= 28) {
                            dd -= 1;
                            result1.innerHTML = ` <h2 class=" mt-3 text-danger text-left ">
                            ${dd} / ${mm} / ${yy}
                           </h2>`
                        } else {
                            alert("Dữ Liệu Không Hợp Lệ");
                        }
                    }

                } else if (mm == 8) {
                    if (dd == 1) {
                        dd = 31;
                        mm -= 1;
                        result1.innerHTML = ` <h2 class=" mt-3 text-danger text-left ">
                        ${dd} / ${mm} / ${yy}
                       </h2>`
                    } else if (1 < dd && dd <= 31) {
                        dd -= 1;
                        result1.innerHTML = ` <h2 class=" mt-3 text-danger text-left ">
                        ${dd} / ${mm} / ${yy}
                       </h2>`
                    } else {
                        alert("Dữ Liệu Không Hợp Lệ");
                    }
                } else {
                    if (dd == 1) {
                        dd = 31;
                        mm -= 1;
                        result1.innerHTML = ` <h2 class=" mt-3 text-danger text-left ">
                        ${dd} / ${mm} / ${yy}
                       </h2>`
                    } else if (1 < dd && dd <= 30) {
                        dd -= 1;
                        result1.innerHTML = ` <h2 class=" mt-3 text-danger text-left ">
                        ${dd} / ${mm} / ${yy}
                       </h2>`
                    } else {
                        alert("Dữ Liệu Không Hợp Lệ");
                    }
                }

            }
        } else {
            alert("Dữ Liệu Không Hợp Lệ");
        }

    } else {
        alert("Năm Cần Lớn Hơn 1920");
        alert("Dữ Liệu Không Hợp Lệ");
    }
}

function ngayMai() {
    var dd = document.getElementById("txt-ngay").value * 1;
    var mm = document.getElementById("txt-thang").value * 1;
    var yy = document.getElementById("txt-nam").value * 1;
    var result1 = document.getElementById("result1");
    if (yy >= 1920) {
        if (1 <= mm && mm <= 12) {
            if (mm == 4 || mm == 6 || mm == 9 || mm == 11) {
                if (dd == 30) {
                    dd = 1;
                    mm += 1;
                    result1.innerHTML = ` <h2 class=" mt-3 text-danger text-left ">
                    ${dd} / ${mm} / ${yy}
                   </h2>`
                } else if (1 <= dd && dd < 30) {
                    dd += 1;
                    result1.innerHTML = ` <h2 class=" mt-3 text-danger text-left ">
                    ${dd} / ${mm} / ${yy}
                   </h2>`
                } else {
                    alert("Dữ Liệu Không Hợp Lệ");
                }
            } else {
                if (mm == 2) {
                    if ((yy % 4 == 0 && yy % 100 != 0) || yy % 400 == 0) {
                        if (dd == 29) {
                            dd = 1;
                            mm += 1;
                            result1.innerHTML = ` <h2 class=" mt-3 text-danger text-left ">
                            ${dd} / ${mm} / ${yy}
                           </h2>`
                        } else if (1 <= dd && dd < 29) {
                            dd += 1;
                            result1.innerHTML = ` <h2 class=" mt-3 text-danger text-left ">
                            ${dd} / ${mm} / ${yy}
                           </h2>`
                        } else {
                            alert("Dữ Liệu Không Hợp Lệ");
                        }
                    } else {
                        if (dd == 28) {
                            dd = 1;
                            mm += 1;
                            result1.innerHTML = ` <h2 class=" mt-3 text-danger text-left ">
                            ${dd} / ${mm} / ${yy}
                           </h2>`
                        } else if (1 <= dd && dd < 28) {
                            dd += 1;
                            result1.innerHTML = ` <h2 class=" mt-3 text-danger text-left ">
                            ${dd} / ${mm} / ${yy}
                           </h2>`
                        } else {
                            alert("Dữ Liệu Không Hợp Lệ");
                        }
                    }
                } else if (mm == 12) {
                    if (dd == 31) {
                        dd = 1;
                        mm = 1;
                        yy += 1;
                        result1.innerHTML = ` <h2 class=" mt-3 text-danger text-left ">
                        ${dd} / ${mm} / ${yy}
                       </h2>`
                    } else if (1 <= dd && dd < 31) {
                        dd += 1;
                        result1.innerHTML = ` <h2 class=" mt-3 text-danger text-left ">
                        ${dd} / ${mm} / ${yy}
                       </h2>`
                    } else {
                        alert("Dữ Liệu Không Hợp Lệ");
                    }
                } else {
                    if (dd == 31) {
                        dd = 1;
                        mm += 1;
                        result1.innerHTML = ` <h2 class=" mt-3 text-danger text-left ">
                        ${dd} / ${mm} / ${yy}
                       </h2>`
                    } else if (1 <= dd && dd < 31) {
                        dd += 1;
                        result1.innerHTML = ` <h2 class=" mt-3 text-danger text-left ">
                        ${dd} / ${mm} / ${yy}
                       </h2>`
                    } else {
                        alert("Dữ Liệu Không Hợp Lệ");
                    }
                }
            }
        } else {
            alert("Dữ Liệu Không Hợp Lệ");
        }
    } else {
        alert("Năm Cần Lớn Hơn 1920");
        alert("Dữ Liệu Không Hợp Lệ");
    }
}

/** exercise 2 */

function tinhNgay() {
    var thangEl = document.getElementById("txt-so-thang").value * 1;
    var namEl = document.getElementById("txt-so-nam").value * 1;
    var ngayEl = 0;
    var result2 = document.getElementById("result2");
    if (namEl >= 1920) {
        if (1 <= thangEl && thangEl <= 12) {
            if (thangEl == 2) {
                if ((namEl % 4 == 0 && namEl % 100 != 0) || namEl % 400 == 0) {
                    ngayEl = 29;
                    var result2 = document.getElementById("result2");
                    result2.innerHTML = ` <h2 class=" mt-3 text-danger text-left ">
                    Tháng ${thangEl} Năm ${namEl} có ${ngayEl} Ngày
                    </h2>`
                } else {
                    ngayEl = 28;
                    result2.innerHTML = ` <h2 class=" mt-3 text-danger text-left ">
                    Tháng ${thangEl} Năm ${namEl} có ${ngayEl} Ngày
                    </h2>`
                }

            } else if (thangEl == 4 || thangEl == 6 || thangEl == 9 || thangEl == 11) {
                ngayEl = 30;
                result2.innerHTML = ` <h2 class=" mt-3 text-danger text-left ">
                Tháng ${thangEl} Năm ${namEl} có ${ngayEl} Ngày
                </h2>`
            } else {
                ngayEl = 31;
                result2.innerHTML = ` <h2 class=" mt-3 text-danger text-left ">
                Tháng ${thangEl} Năm ${namEl} có ${ngayEl} Ngày
                </h2>`
            }
        } else {
            alert("Dữ Liệu Không Hợp Lệ");
        }
    } else {
        alert("Năm Cần Lớn Hơn 1920");
        alert("Dữ Liệu Không Hợp Lệ");
    }
}

/** exercise 3 */

function docSo() {
    var soNguyen = document.getElementById("txt-soNguyen").value * 1;
    var a = Math.floor(soNguyen / 100);
    var b = Math.floor(soNguyen / 10) % 10;
    var c = soNguyen % 10;
    var resulta = '';
    var resultb = '';
    var resultc = '';
    if (100 > soNguyen || soNguyen > 999) {
        alert("Dữ Liệu Không Hợp Lệ");
    } else {
        if (b == 0 && c == 0) {
            switch (a) {
                case 1: {
                    resulta = 'Một Trăm';
                }; break;
                case 2: {
                    resulta = 'Hai Trăm';
                }; break;
                case 3: {
                    resulta = 'Ba Trăm';
                }; break;
                case 4: {
                    resulta = 'Bốn Trăm';
                }; break;
                case 5: {
                    resulta = 'Năm Trăm';
                }; break;
                case 6: {
                    resulta = 'Sáu Trăm';
                }; break;
                case 7: {
                    resulta = 'Bảy Trăm';
                }; break;
                case 8: {
                    resulta = 'Tám Trăm';
                }; break;
                case 9: {
                    resulta = 'Chín Trăm';
                }; break;
            }
            document.getElementById("result3").innerHTML =
                ` <h2 class=" mt-3 text-danger text-left ">
                 ${resulta}
         </h2>`
        } else {
            switch (a) {
                case 1: {
                    resulta = 'Một Trăm';
                }; break;
                case 2: {
                    resulta = 'Hai Trăm';
                }; break;
                case 3: {
                    resulta = 'Ba Trăm';
                }; break;
                case 4: {
                    resulta = 'Bốn Trăm';
                }; break;
                case 5: {
                    resulta = 'Năm Trăm';
                }; break;
                case 6: {
                    resulta = 'Sáu Trăm';
                }; break;
                case 7: {
                    resulta = 'Bảy Trăm';
                }; break;
                case 8: {
                    resulta = 'Tám Trăm';
                }; break;
                case 9: {
                    resulta = 'Chín Trăm';
                }; break;
            }
            switch (b) {
                case 0: {
                    resultb = 'lẻ';
                }; break;
                case 1: {
                    resultb = 'Mười';
                }; break;
                case 2: {
                    resultb = 'Hai Mươi';
                }; break;
                case 3: {
                    resultb = 'Ba Mươi';
                }; break;
                case 4: {
                    resultb = 'Bốn Mươi';
                }; break;
                case 5: {
                    resultb = 'Năm Mươi';
                }; break;
                case 6: {
                    resultb = 'Sáu Mươi';
                }; break;
                case 7: {
                    resultb = 'Bảy Mươi';
                }; break;
                case 8: {
                    resultb = 'Tám Mươi';
                }; break;
                case 9: {
                    resultb = 'Chín Mươi';
                }; break;
            }
            switch (c) {
                case 1: {
                    resultc = 'Một';
                }; break;
                case 2: {
                    resultc = 'Hai';
                }; break;
                case 3: {
                    resultc = 'Ba';
                }; break;
                case 4: {
                    resultc = 'Bốn';
                }; break;
                case 5: {
                    resultc = 'Năm';
                }; break;
                case 6: {
                    resultc = 'Sáu';
                }; break;
                case 7: {
                    resultc = 'Bảy';
                }; break;
                case 8: {
                    resultc = 'Tám';
                }; break;
                case 9: {
                    resultc = 'Chín';
                }; break;
            }
            document.getElementById("result3").innerHTML =
                ` <h2 class=" mt-3 text-danger text-left ">
                 ${resulta} ${resultb} ${resultc}
         </h2>`
        }
    }
}

/** exercise 4 */
function tinhChieuDai (a1,a2,b1,b2){
    var d = Math.sqrt( (a1-a2)*(a1-a2) + (b1-b2)*(b1-b2));
    return d; 
 }
function tim(){
    var sv1 = document.getElementById("txt-Sv-1").value;
    var sv2 = document.getElementById("txt-Sv-2").value;
    var sv3= document.getElementById("txt-Sv-3").value;

    var x = document.getElementById("txt-X").value*1;
    var x1 = document.getElementById("txt-X1").value*1;
    var x2 = document.getElementById("txt-X2").value*1;
    var x3 = document.getElementById("txt-X3").value*1;

    var y = document.getElementById("txt-Y").value*1;
    var y1 = document.getElementById("txt-Y1").value*1;
    var y2 = document.getElementById("txt-Y2").value*1;
    var y3 = document.getElementById("txt-Y3").value*1;

    var d1= tinhChieuDai(x,x1,y,y1);
    console.log("🚀 ~ file: index.js:459 ~ tim ~ d1", d1)
    var d2= tinhChieuDai(x,x2,y,y2);
    console.log("🚀 ~ file: index.js:461 ~ tim ~ d2", d2)
    var d3= tinhChieuDai(x,x3,y,y3);
    console.log("🚀 ~ file: index.js:463 ~ tim ~ d3", d3)

    var result4 = document.getElementById("result4");

    if(d1>d2&& d1>d3){
        result4.innerHTML=`
        <h2 class="  text-danger text-left ">
         Sinh Viên Xa Trường Nhất Là: ${sv1}
         </h2> `
    }else if(d2>d1&& d2>d3){
        result4.innerHTML=`
        <h2 class="  text-danger text-left ">
         Sinh Viên Xa Trường Nhất Là: ${sv2}
         </h2> `
    }else if(d3>d1&& d3>d2){
        result4.innerHTML=`
        <h2 class="  text-danger text-left ">
        Sinh Viên Xa Trường Nhất Là: ${sv3}
         </h2> `
    }else if(d1==d2 && d1>d3){
        result4.innerHTML=`
        <h2 class="  text-danger text-left ">
        Sinh Viên Xa Trường Nhất Là: ${sv1} và ${sv2}
         </h2> `
    }else if(d3==d2 && d2>d1){
        result4.innerHTML=`
        <h2 class="  text-danger text-left ">
        Sinh Viên Xa Trường Nhất Là: ${sv2} và ${sv3}
         </h2> `
    }else if(d1==d3 && d1>d2){
        result4.innerHTML=`
        <h2 class="  text-danger text-left ">
        Sinh Viên Xa Trường Nhất Là: ${sv1} và ${sv3}
         </h2> `
    }else{
        result4.innerHTML=`
        <h2 class="  text-danger text-left ">
        Quảng Đường Đến Trường Của 3 Sinh Viên Bằng Nhau
         </h2> `
    }

}